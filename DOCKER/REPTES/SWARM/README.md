# DOCKER SWARM

## 1. CREAR EL SWARM

### 1.1 Crear-ho.

> Iniciar el docker swarm.

> ```
> $ docker swarm init
> ```

>> ```
>> Swarm initialized: current node (d2zq8lzvizkz3ont6a8d2xhea) is now a manager.
>> To add a worker to this swarm, run the following command:
>>
>>    docker swarm join --token SWMTKN-1-63rcmekvlw95gp9yttp2eauosz4aja66i6imk9gmhei5hqnimj->> a3eclt5pl1vqj8yxu6iyb2as0 10.200.245.217:2377
>>
>> To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
>>
>> ```

### 1.2 Mirar l'estat del docker.

> Mirar l'estat del docker swarm.

> ```
> $ docker info
> ```

### 1.3 Mostrar informació del nodes.

> ```
> $ docker node ls
> ```