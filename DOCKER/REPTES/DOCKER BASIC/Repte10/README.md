# REPTE 9 -> Comptador de visites

Implementar l’exemple de la documentació del comptador de visites.

## Creació de l'arxiu packgage.json

```
{
  "name": "101-app",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "prettify": "prettier -l --write \"**/*.js\"",
    "test": "jest",
    "dev": "nodemon src/index.js"
  },
  "dependencies": {
    "express": "^4.17.1",
    "mysql": "^2.17.1",
    "sqlite3": "^5.0.0",
    "uuid": "^3.3.3",
    "wait-port": "^0.2.2"
  },
  "resolutions": {
    "ansi-regex": "5.0.1"
  },
  "prettier": {
    "trailingComma": "all",
    "tabWidth": 4,
    "useTabs": false,
    "semi": true,
    "singleQuote": true
  },
  "devDependencies": {
    "jest": "^27.2.5",
    "nodemon": "^2.0.13",
    "prettier": "^1.18.2"
  }
}
```
## Creació del Docker file.

```
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

## Arxius que ha de contenir el directory del Repte10.

> *· README.md*

> *· Dockerfile*

> *· package.json*

> *· yarn.lock*

> *· folder -> src*

> *· folder -> spec*

## Construir l'imatge del container.

```
$ docker build -t getting-started .
```

## Encendre el container.

```
$ docker run -dp 3000:3000 getting-started
```

## Comprovació servei web.

*· Accedir pel browser.*

```
http://localhost:3000
```

## ACTUALITZAR APP

### Actualitzar l'arxiu *src/static/js/app.js*.

```
 -                <p className="text-center">No items yet! Add one above!</p>
 +                <p className="text-center">You have no todo items yet! Add one above!</p>
```

### Actualitzar container.

> __ATURAR.__

> ```
> $ docker stop getting-started
> ```

> __ELIMAR IMATGE.__

> ```
> $ docker rmi -f getting-started:latest
> ```

> __CONSTRUIR DE NOU L'IMATGE.__

> ```
> $ docker build -t getting-started .
> ```

> __ENCENDRE CONTAINER.__

> ```
> $ docker run -dp 3000:3000 getting-started
> ```

> __COMPROVACIÓ.__ -> *Accedir pel browser.*

>> ```
>> http://localhost:3000
>> ```
