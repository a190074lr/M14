# REPTE 8 -> Exemple ldap + phpldapadmin

Implementar un docker compose que arranqui el ldap-server(amb persistencia de dades) i l'eina gràfica phpldapadmin.

## CREAR EL DOCKER COMPOSE

```
version: "2"
services:
  ldap:
    image: a190074lr/ldap22:base
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    volumes:
      - "ldap-config:/etc/ldap/slapd.d"
      - "ldap-data:/var/lib/ldap"
    networks:
      - 2hisx
  phpldapadmin:
    image: a190074lr/ldap22:phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - 2hisx
networks:
  2hisx:
volumes:
  ldap-config:
  ldap-data:
```

## ENCENDRA EL DOCKER COMPOSE

```
$ docker compose up -d
```

**Comprovar el funcionament dels dockers.**

```
$ docker ps
```

**Comprovar si la BD esta funcionant correctament.**

```
$ ldapsearch -x -LLL -b 'dc=edt,dc=org' dn
```
