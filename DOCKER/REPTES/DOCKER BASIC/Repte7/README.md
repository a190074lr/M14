
# REPTE 7 -> Creació de servidors: ssh, samba i net

Crear els servidors segünts:

>   · Net: Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.

## EDITAR EL SCRIPT STARTUP.SH DEL LDAP:BASE

Primer de tot editem el Dockerfile:

```
FROM debian:latest
LABEL author="@edt ASIX Curs 2022"
LABEL description="xinetd Server Curs 2022"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y xinetd procps iproute2 iputils-ping nmap net-tools apache2 tftp-server ftp
WORKDIR /opt/docker/
COPY daytime-stream chargen-strem echo-stream /etc/xinetd.d
CMD [ "/usr/sbin/xinetd","-dontfork" ]
EXPOSE 7 13 19
```