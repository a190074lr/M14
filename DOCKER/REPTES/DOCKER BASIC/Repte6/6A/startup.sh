#! /bin/bash
# @edt ASIX M14-PROJECTES
# Octubre 2022
# Descripció: Implementar un servidor LDAP on el nom de l’administrador de la base de dades de
#l’escola sigui captat a través de variables d’entorn.
#
# ------------------------------

echo "Inicialització BD ldap edt.org"

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
sed -i "s/Manager/$ADMIN_NAME/" slapd.conf
sed -i "s/rootpw secret/rootpw $ADMIN_PASSWD/" slapd.conf
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
