# REPTE 6A -> Networks, Variables i Secrets

Implementar un servidor LDAP on el nom de l’administrador de la base de dades de
l’escola sigui captat a través de variables d’entorn.

## EDITAR EL SCRIPT STARTUP.SH DEL LDAP:BASE

Editem el script per tal de que modifiqui el nom i la contraseña de l'administrador segons la variable que introduïm pel __docker run__.

```
echo "Inicialització BD ldap edt.org"

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
sed -i "s/Manager/$ADMIN_NAME/" slapd.conf
sed -i "s/secret/$ADMIN_PASSW/" slapd.conf
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```

>*__sed -i "s/Manager/$ADMIN_NAME/" slapd.conf__ -> Reemplaça en el fitxer slapd.conf, Manager per la variable __$ADMIN_NAME__, que tindrà el valor que li assignem duran el docker run.*

>*__sed -i "s/Manager/$ADMIN_PASSW/" slapd.conf__ -> Reemplaça en el fitxer slapd.conf, secret per la variable __$ADMIN_PASSW__, que tindrà el valor que li assignem duran el docker run.*

## ENGEGAR EL CONTAINER AMB LES VARIABLES D'ENTORN

Engeguem el container assignant-li les variables d'entorn __$ADMIN_NAME__ y $ADMIN_PASSW.

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -e ADMIN_NAME="Lucas" -e ADMIN_PASSW="lucas" -d a190074lr/m14_projectes:repte6A 
```