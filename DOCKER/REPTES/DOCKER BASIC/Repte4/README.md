# REPTE 4 -> Servidor LDAP amb persistència de dades

>*Implementar una imatge que actui com a servidor lda pamb persistència de dades tant de configuració com les dades de la base de dades d’usuaris*

## ENGEGAR CONTAINER AMB LA CREACIÓ DE VOLUMS PERTINENTS

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/ldap22:base
```

> *El __-v__ ens crea de manera directa el docker volumen; el volumen __ldap-config__ l'encarregat d'emmagatzemar el directori de configuració, i el volumen __ldap-data__ l'encarregat d'emmagatzemar les dades de la BD.*