# REPTE 5 -> Servidor LDAP amb entrypoint (configurable)

Implementar una imatge de servidor ldap configurable segons el paràmetre rebut.S’executa amb un entrypoint que és un script que actuarà segons li indiqui l’argument rebut:

>   · initdb → ho inicialitza tot de nou i fa el populate de edt.org

>   · slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

>   · start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

>   · slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada. 

## ENGEGAR CONTAINER AMB LA CREACIÓ DE VOLUMS PERTINENTS I AMB ARGUMENTS


*1. Engegar el docker amb l'argument __initdb__ que corresponent a carregar les dades de la BD, des de 0 ( en aquest cas per primer cop ).*

> *El __-v__ ens crea de manera directa el docker volumen; el volumen __ldap-config__ l'encarregat d'emmagatzemar el directori de configuració, i el volumen __ldap-data__ l'encarregat d'emmagatzemar les dades de la BD.*

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/m14_projectes:repte5 initdb
```

> *Comprovar l'existéncia de la BD:*

```
$ ldapsearch -x -LLL -b 'dc=edt,dc=org' dn
```

2. Tornar a inicialitzar el docker peró amb l'argument __slapd__, ho inicialitza tot però només engega el servidor, sense posar-hi dades.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/m14_projectes:repte5 slapd
```

> *Des de dins del docker comprovem que si esta implementat el fixer de configuració peró no les dades de la BD:*

```
$ docker exec -it ldap.edt.org /bin/bash
```

```
root@ldap:/opt/docker# tree /etc/ldap/slapd.d/
/etc/ldap/slapd.d/
|-- cn=config
|   |-- cn=module{0}.ldif
|   |-- cn=schema
|   |   |-- cn={0}corba.ldif
|   |   |-- cn={10}ppolicy.ldif
|   |   |-- cn={11}collective.ldif
|   |   |-- cn={1}core.ldif
|   |   |-- cn={2}cosine.ldif
|   |   |-- cn={3}duaconf.ldif
|   |   |-- cn={4}dyngroup.ldif
|   |   |-- cn={5}inetorgperson.ldif
|   |   |-- cn={6}java.ldif
|   |   |-- cn={7}misc.ldif
|   |   |-- cn={8}nis.ldif
|   |   `-- cn={9}openldap.ldif
|   |-- cn=schema.ldif
|   |-- olcDatabase={-1}frontend.ldif
|   |-- olcDatabase={0}config.ldif
|   |-- olcDatabase={1}mdb.ldif
|   `-- olcDatabase={2}monitor.ldif
`-- cn=config.ldif

2 directories, 19 files
```

3. Tornar a inicialitzar el docker peró amb l'argument __slapcat nº__, fa un slapcat de la base de dades indicada.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it a190074lr/m14_projectes:repte5 slapcat 1
```

4. Tornar a inicialitzar el docker amb un dels 3 arguments següents; *start,edtorg,null*. I comprovar-ne la persisténcia de dades:

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/m14_projectes:repte5 start
```

> *Fem alguna modificació a la BD per veurse si posteriorment funciona la persiténcia de dades.*

```
$ ldapsearch -x -LLL -b 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org'

dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pere Pou
sn: Pou
homePhone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
```

```
$ cat mod.ldif
dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: mail
mail: noumail@edt.org
```
```
$ ldapmodify -vx -D 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org' -w pere -f mod.ldif
ldap_initialize( <DEFAULT> )
replace mail:
	noumail@edt.org
modifying entry "cn=Pere Pou,ou=usuaris,dc=edt,dc=org"
modify complete
```

> __4.2__ *Aturem el docker i el turnem a encendre amb un argument diferent:*

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d a190074lr/m14_projectes:repte5 edtorg
```

> *Comprovem si hi ha persisténcia de dades.*

```
$ ldapsearch -x -LLL -b 'cn=Pere Pou,ou=usuaris,dc=edt,dc=org'

dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pere Pou
sn: Pou
homePhone: 555-222-2221
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=
mail: noumail@edt.org
```






























