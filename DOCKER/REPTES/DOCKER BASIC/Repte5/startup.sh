#! /bin/bash
# @edt ASIX M14-PROJECTE
# SEPTEMBRE 2022
# Descripció: Fer un programa que incia la BD ldap i el seu dimoni corresponent, segons l'argument introduït un ho fa amb certes característiques corresponents.
#
#		initdb → ho inicialitza tot de nou i fa el populate de edt.org.
#		slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades.
#		start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents.
#		slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 
#
#	$prog arg_ldap
# ------------------------------

arg=$1

# Validar si hi ha algún argument.

if [ $# -lt 1 ]; then
  arg="null"
fi

# Incialitzar la BD ldap segons l'argument introduït i les seves característiques corresponents.

case $arg in
	"initdb")
		echo "Inicialització BD ldap edt.org"
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
		chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;

	"slapd")
		echo "Inicialització BD ldap edt.org"
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;

	"slapcat")
		if [ $# -eq 1 ];then
			/usr/sbin/slapcat
		else
			lvl=$2
			/usr/sbin/slapcat -n $lvl
		fi;;
		
	"start"|"edtorg"|"null")
		/usr/sbin/slapd -d0;;
esac
exit 0	
