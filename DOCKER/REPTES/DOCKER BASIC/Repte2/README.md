#M14 PROJECTES

##REPTE 2

Generar un servidor web usant Apache que funcioni en detach. Cal fer la propagació
del port del container al port del host amfitrió.



	- $ docker build -t a190074lr/m14_projectes:repte2 .
	- $ docker run --rm -p 80:80 -d a190074lr/m14_projectes:repte2
	- $ docker ps
	- Buscar en google, localhost:80
	- O altre opció:
		+ $ wget 172.17.0.2
		+ $ cat index. html
