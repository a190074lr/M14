#M14 PROJECTES

## REPTE 1

Generar una imatge personalitzada del sistema operatiu Debian amb els paquets
necessaris per poder inspeccionar el sistema. Usar un Dockerfile per automatitzar la
generació. Ha de permetre realitzar les ordres: ps, ping, ip, nmap, tree, vim com a
mínim.
