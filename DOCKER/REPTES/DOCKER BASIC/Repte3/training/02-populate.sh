#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	\copy clientes from /docker-entrypoint-initdb.d/clientes.dat
        \copy repventas from /docker-entrypoint-initdb.d/repventas.dat
        \copy pedidos from /docker-entrypoint-initdb.d/pedidos.dat
        \copy oficinas from /docker-entrypoint-initdb.d/oficinas.dat
        \copy productos from /docker-entrypoint-initdb.d/productos.dat
EOSQL
