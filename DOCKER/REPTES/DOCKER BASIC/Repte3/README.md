# Lucas Rodríguez Cabañeros


# REPTE 3

> *Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge postgres original amb populate i persistència de dades. Exemples de SQL injectat usant volumes.*

## IMATGE POSTGRES OFICIAL

> *Per obtenir l'imatge de postgres oficial la trobem a:*

```
/library/postgres
```

## ENGEGAR CONTAINER PER DESPLEGAR EL POSTGRES

```
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd)/training:/docker-entrypoint-initdb.d -v postgres-data:/var/lib/postgresql/data/ --net 2hisx -d a190074lr/m14_projectes:repte3
```

> *El -v $(pwd)/training:/docker-entrypoint-initdb.d amb d'inicar el servei psql, executará qualsevol script que hi hagi en el directori /docker-entrypoint-initdb.d en aquest cas el populate.sh.*
> *El __-v__ ens crea de manera directa el docker volumen ( s'anomenar __postgres-data__ ), que serà l'encarregat d'emmagatzemar les modificacions que és facin en la BD training.*

## COMPROVACIÓ FUNCIONAMENT BD

> *Entrar directament amb docker exec i realitzar una sessió de l'interpret SQL psql, en ser local no cal passwd.*

```
$ docker exec -it training  psql -U postgres -d training
```

> *Comprovar l'existencia de la BD*

```
training=# \d
```

> *Esborrar-ne una taula qualsevol en aquest cas __oficinas__.*

```
training=# DROP TABLE oficinas;
```

> *Surtir del docker amb __CTRL+D__.*

>  *Aturar el docker amb __$ docker stop postgres22__.*

## COMPROVACIÓ FUNCIONAMENT DADES PERSISTENTS

> *Tornar-hi a encendre el docker a partir del mateix docker volumen d'abans, en aquest cas __postgres-data__*

```
$ docker run --rm --name postgres22 -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres-data:/var/lib/postgresql/data/ --net 2hisx -d a190074lr/m14-projecte:repte3
```

> *Treiem el -v $(pwd)/training:/docker-entrypoint-initdb.b ja que la BD no estará buida, per tan ignorarà qualsevol script que hi hagi en el directori.*
> *Comprovar la persistencia de dades de la BD, en aquest cas comprovant si continua estant eliminada la taula __oficinas__.*


```
training=# \d
```

> *Per comprovar des de el localhost*

```
$ psql -h 172.19.0.2 -U postgres -d training -c "SELECT*FROM oficinas;"
```

 
