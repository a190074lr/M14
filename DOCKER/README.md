# REPTE 10 -> Comptador de visites

Implementar l’exemple de la documentació del comptador de visites.

## Creació del programa *package.json*

```
{
  "name": "101-app",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "prettify": "prettier -l --write \"**/*.js\"",
    "test": "jest",
    "dev": "nodemon src/index.js"
  },
  "dependencies": {
    "express": "^4.17.1",
    "mysql": "^2.17.1",
    "sqlite3": "^5.0.0",
    "uuid": "^3.3.3",
    "wait-port": "^0.2.2"
  },
  "resolutions": {
    "ansi-regex": "5.0.1"
  },
  "prettier": {
    "trailingComma": "all",
    "tabWidth": 4,
    "useTabs": false,
    "semi": true,
    "singleQuote": true
  },
  "devDependencies": {
    "jest": "^27.2.5",
    "nodemon": "^2.0.13",
    "prettier": "^1.18.2"
  }
}
```

## Creació del *Dockerfile*

```
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

## CONTAINER

> Construir l'imatge.

> ```
> $ docker build -t getting-started .
> ```

> Engegar el container.

> ```
> $ docker build -t getting-started .
> ```