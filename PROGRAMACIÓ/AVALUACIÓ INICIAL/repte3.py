#                    REPTE 3 
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   Desenvolupar un programa que determini si un fitxer es troba (o existeix) en un
#   directori donat o no. 
# 
#       - Si el fitxer existeix, llegir-hi les primeres cinc línies. 
#       - Si el fitxer no existeix, demanar a l’usuari un nom per al fitxer i crear-lo en el directori corresponent.
#
#  usage: python3 repte3.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# Definim la funció que farà "head".

def head(fluxe_fitxer):
    '''
    Funció que fa el head
    Input:Fluxe de fitxer
    Output: Res
    '''
    cont=1
    while cont <=int(5):
        linea=fluxe_fitxer.readline()
        cont=cont+1
        print(linea.strip())

# Definim la variable fitxer que contindrà el suposat nom del fitxer introduït per argument.
fitxers=sys.argv[1]

# CONTROL D'ERRORS

# Si no troba cap error, fa la funció "head".
try:
    fluxe_fitxer=open(fitxers, 'r')
    head (fluxe_fitxer)
    fluxe_fitxer.close()

# Si troba l'error "FileNotFoundError" que vol dir que el fitxer no existeix, mostra un missatge
# d'error per pantalla i demana per linea de comandes el nom per crear-ho.

except FileNotFoundError:
    print("ERROR, el fitxer",fitxers,"no existeix, així que el creem.")
    new_file = fitxers
    print(new_file)
    new_file = open(new_file, "w")

# Potser es podría posar un control d'errors alhora de no rebre cap argument.