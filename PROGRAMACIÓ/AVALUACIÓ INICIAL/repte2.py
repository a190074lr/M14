#                    REPTE 2 
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   Desenvolupar un programa que creï un fitxer de text, que es guardi amb el nom
#   corresponent al que hi posem com a primer argument i que com a contingut tingui el que
#   hi posem al segon argument.
#
#  usage: python3 repte2.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# Declaració de variables.

nom = sys.argv[1]

# Obrim el fitxer amb el nom introduït per agument, en mode escriptura.

fitxer = open(nom, "w")

# Las lines tindràn el contingut del segon argument.

fitxer.write(sys.argv[2])
fitxer.close()