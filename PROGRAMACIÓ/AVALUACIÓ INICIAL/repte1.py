#                    REPTE 1 
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   Desenvolupar un programa que creï un fitxer de text buit.
#
#  usage: python3 repte1.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Obrir el fitxer en mode escriptura amb el nom "fitxer.txt".

fitxer = open( 'fitxer.txt', 'w' )

# Tancar el fitxer.

fitxer.close()

