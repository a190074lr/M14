#                    REPTE 4 
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   Desenvolupar un programa on s’hi introdueixi el nom d’un fitxer, amb el seu
#   directori complet corresponent. 
# 
#       - Si el fitxer no existeix, el programa avisa a l’usuari que el
#         fitxer no existeix o no es troba al directori introduït. 
#       - Si el fitxer existeix, el programa copia tot el contingut d’aquest 
#         en un nou fitxer anomenat com l’original però situat en un
#         directori un nivell més aprop al directory root. Si el fitxer original ja es troba al directori
#         root, el programa no fa res.
#
#  usage: python3 repte4.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# CONTROL D'ERRORS

# Si no troba cap error, fa la funció "head".

try:

    ruta_absoluta=sys.argv[1]

    elements_ruta=ruta_absoluta.split("/")
    nom_fitxer=elements_ruta[-1]
    nou_dir="/".join(elements_ruta[:-2])
    ruta_nova=nou_dir + "/" + nom_fitxer

    ff=open(r'{}'.format(ruta_absoluta),'r')
    lines=[]
    for oneLine in ff:
        lines.append(oneLine[:-1])    
    ff.close
    ff=open(r'{}'.format(ruta_nova),'w')
    text="\n".join(lines)
    ff.write(text)
    ff.close

# Si troba l'error "FileNotFoundError" que vol dir que el fitxer no existeix, mostra un missatge
# d'error per pantalla i demana per linea de comandes el nom per crear-ho.

except FileNotFoundError:
    print("ERROR: El fitxer no existeix.")


