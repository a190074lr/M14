#                    REPTE 5 
#!/usr/bin/python3
#-*- coding: utf-8-*-
#
#   Desenvolupar un programa que llegeixi aquest document, introduint-hi el directori
#   complet que correspon a aquest fitxer i que, en el mateix directori creï un fitxer anomenat
#   
#   “Llista_usuaris.txt”. Aquest nou fitxer ha de contenir quatre línies: 
# 
#       - La primera comença amb Noms: seguit de tots els noms d’usuari del fitxer “Usuaris.txt”, separant el nom de
#         cada usuari amb una coma “,”. 
#       - La segona línia comença amb Contrasenyes.
#       - La tercera amb Edats. L
#       - La quarta amb Administradors.
# 
#   Seguint la mateixa estructura que presenta la línia Noms.
#   
#   - Si el fitxer “Usuaris.txt” no existeix, el programa ha d’informar a l’usuari
#     d’aquest fet sense que es generi un error que aturi l’execució d’aquest
#
#  usage: python3 repte5.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# Definim la funció encarregada de mostrar la cadena en format correcte.

def creacio_cadena (noms, passwd, edat, admin):
    '''
	'''
    cadena_final = noms[:-1] + '\n' + passwd[:-1] + '\n' + edat[:-1] + '\n' + admin[:-1]
    return cadena_final

# Definim la funció encarregada de crear el fitxer amb el nom "llista-usuaris.txt".

def crea_fitxer (cadena):
    '''
	'''
    with open ('llista-usuaris.txt', 'w') as f_llista:
            f_llista.write(cadena)
    return None

# Definim la funció encarregada per crear la llista.

def crea_llista(f_f):
    '''
	Funció que donat un fitxer d'usuaris determinat escriu una llista
	de la seguent estructura:
		Noms: nom1, nom2, nom3
		Contrasenyes: pw1 ,pw2 ,pw3
		Edats: edat1, edat2, edat3
		Administradors: si, no, si, no
	'''
    noms = 'Noms: '
    passwd = 'Contrasenyes: '
    edat = 'Edats: '
    admin = 'Administradors: '
    element = f_f.readlines()
    tot = len(element)
    for user in element:
        tot = tot - 1
        usuari = user.split(',')
        noms = noms + usuari[0] + ','
        passwd = passwd + usuari[1] + ','
        edat = edat + usuari[2] + ','
        admin = admin + usuari[3][:-1] + ','
        if tot == 0:
            cadena = creacio_cadena(noms,passwd,edat,admin)
            crea_fitxer(cadena)

# CONTROL D'ERRORS

# Si no troba cap error, fa la funció fitxer_creat, que es crear el fitxer i mostras un missatge si s'ha creat correctament.

try:
	fitxer_creat= 'llista-usuaris.txt'
	file = sys.argv[1]
	with open (file, 'r') as fitxer:
		crea_llista(fitxer)
	print('Fitxer', fitxer_creat,  'creat')

# Si troba l'error "FileNotFoundError" que vol dir que el fitxer no existeix, mostra un missatge
# d'error per pantalla i demana per linea de comandes el nom per crear-ho.

except FileNotFoundError:
	print('ERROR!! El fitxer', file, 'no existeix')
