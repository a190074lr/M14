#!/usr/bin/python3
#-*- coding: utf-8-*-
#                    REPTE 1 
#
#   Llegeix un fitxer i el mostra per l’standard output. Entrem el nom del fitxer com a argument.
#
#  usage: ./repte1.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# Definir variables d'error.

no_file = 1
no_argv = 2
no_dir = 3
no_code = 4
no_perm = 5 

# Definir funció encarregada de fer "cat".

def cat(fluxe_fitxer):
	''' 
    Funció que mostra un fitxer per stdout (sortida stàndard)
	Entrada: Fluxe de fitxer
	Sortida: res
	'''
	#llegeixo i mostro
	for linia in fluxe_fitxer:
		#és el mateix que les dues línies següents
		#sys.stdout.write(linia)
		
		linia = linia.strip()	#el mètode strip() 'arregla' la línia ( i treu el intro)
		print(linia)	#la funció print m'afegeix un intro

# CONTROL D'ERRORS

# Si no troba cap error, fa la funció "head".
try:
    fitxer = sys.argv[1]
    fluxe_fitxer=open(fitxer, 'r')
    cat(fluxe_fitxer)
    fluxe_fitxer.close()

# Si troba l'error "FileNotFoundError" que vol dir que el fitxer no existeix, mostra un missatge
# d'error per pantalla i demana per linea de comandes el nom per crear-ho.

except FileNotFoundError:
    print("ERROR, el fitxer",fitxer,"no existeix.")
    sys.exit(no_file)

except IndexError:
    print("ERROR, no s'ha trobar cap argument.")
    sys.exit(no_argv)

except IsADirectoryError:
	print("Error el directori no existeix.")
	sys.exit(no_dir)

except UnicodeDecodeError:
	print("Error en el codi.")
	sys.exit(no_code)

except PermissionError:
	print("No tens permissos per executar-ne l'arxiu", fitxer)
	sys.exit(no_perm)
