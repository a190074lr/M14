#!/usr/bin/python3
#-*- coding: utf-8-*-
#                    REPTE 2 
#
#   Llegeix un fitxer i en mostra les n primeres files. Hem de poder entrar el nom del fitxer com a primer argument i el número de files com a segon, de la mateixa manera
#	que el programa ha de funcionar correctament si introduïm el nombre de files com a primer argument i el nom del fitxer com a segon.
#
#  usage: ./repte2.py
#
# Escola del treball de Barcelona
# ASX 2HISX Curs 2022-2023
# Novembre 2021
#
# Lucas Rodríguez. a190074lr
# 10/11/2023 
#

# Importar els arguments del introduïts en la línea de comandes.

import sys

# Definir variables d'error.

no_file = 1
no_argv = 2
no_line = 3
no_format = 4

# Definir funció encarregada de fer "head".

def head(fluxe_fitxer,max):
    '''
    Funció que fa el head
    Input:Fluxe de fitxer
    Output: Res
    '''
    f_obert= open(fluxe_fitxer,'r')
    cont = 1
    while cont <= int(max):
        linea=f_obert.readline()
        cont=cont+1
        print(linea.strip())

# CONTROL D'ERRORS

# Si no troba cap error, fa la funció "head".
try:
	fitxer = sys.argv[1]
	fluxe_fitxer = open(fitxer, 'r')
	if int(sys.argv[2])	 > len(fluxe_fitxer.readlines()):
		print(sys.argv[2], 'són mes líneas de les que té el fitxer.')
		sys.exit(no_line)

	else:
		fluxe_fitxer.close()
		head(fitxer,sys.argv[2])

# Si troba l'error "FileNotFoundError" que vol dir que el fitxer no existeix, mostra un missatge
# d'error per pantalla i demana per linea de comandes el nom per crear-ho.

except FileNotFoundError:
	try:
		fitxer = sys.argv[2]
		fluxe_fitxer = open(fitxer, 'r')
		if int(sys.argv[1]) > len(fluxe_fitxer.readlines()):
			print(sys.argv[1], 'són mes líneas de les que té el fitxer.')
			sys.exit(no_line)

		else:
			fluxe_fitxer.close()
			head(fitxer,sys.argv[2])

	except FileNotFoundError:
		print('Els arguments', sys.argv[1], 'y', sys.argv[2], 'no son fitxers.')
		sys.exit(no_file)

	except IndexError:

		print("ERROR, no s'ha trobar cap argument.")
		sys.exit(no_argv)

	except ValueError:
		print('El fitxer', fitxer, 'no és un fitxer escriptura.')
		sys.exit(no_format)

except IndexError:

    print("ERROR, no s'ha trobar cap argument.")
    sys.exit(no_argv)

except ValueError:
	print('El fitxer', fitxer, 'no és un fitxer escriptura.')
	sys.exit(no_format)